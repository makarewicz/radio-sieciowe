#include <signal.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "data_packet.h"
#include "data_queue.h"
#include "control_packet.h"
#include "util.h"
#include "resend_queue.h"
#include "nadajnik_info.h"

#define TTL_VALUE 2

/* Parametry konfiguracyjne: */
/* Adres rozgłaszania ukierunkowanego */
static char* MCAST_ADDR = NULL;
/* Port UDP używany do przesyłania danych. */
static int DATA_PORT = 26310;
/* Port UDP używany do transmisji pakietów kontrolnych */
static int CTRL_PORT = 36310;
/* Rozmiar w bajtach paczki. */
static size_t PSIZE = 512;
/* Rozmiar w bajtach kolejki FIFO. */
static size_t FSIZE = 131072;
/* Liczba pakietow trzymanych w kolejce */
static size_t QSIZE;
/* Czas (w milisekundach) pomiędzy wysłaniem kolejnych raportów o brakujących
 * paczkach (dla odbiorników)  oraz czas między kolejnymi retransmisjami
 * paczek. */
static int RTIME = 250;
/* Nazwa nadajnika. */
static char* NAME = "Nienazwany Nadajnik";
/* Application name */
static char* APP_NAME;
/* Version number */
static char* VERSION = "1.0";

volatile static bool finish = false;
static struct data_queue* queue;

static int multicast_socket;
static struct sockaddr_in multicast_addr;

static struct nadajnik_info* info;
static char* serialized_info;
static size_t serialized_info_len;

static volatile struct resend_queue* collecting_queue;
static struct resend_queue* sending_queue;

void sig_handler(int sig) {
    switch (sig) {
        case SIGTERM:
            finish = true;
        default:
            fprintf(stderr, "wasn't expecting that!\n");
            abort();
    }
}

int parse_command_line (int argc, char** argv) {
    int c;
    APP_NAME = argv[0];
    while ((c = getopt (argc, argv, "a:P:C:p:f:R:n:")) != -1)
        switch (c) {
            case 'a':
                MCAST_ADDR = optarg;
                break;
            case 'P':
                DATA_PORT = atoi(optarg);
                break;
            case 'C':
                CTRL_PORT = atoi(optarg);
                break;
            case 'p':
                PSIZE = atoi(optarg);
                break;
            case 'f':
                FSIZE = atoi(optarg);
                break;
            case 'R':
                RTIME = atoi(optarg);
                break;
            case 'n':
                NAME = optarg;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                critical_err("Parsing command line failed");
                return 1;
        }
    if(MCAST_ADDR == NULL) {
        fprintf(stderr, "mcast_address is required argument\n");
        critical_err("Parsing command line failed");
        return 1;
    }
    return 0;
}

void send_packet_on_multicast_addr(struct data_packet* dp) {
    char* serialized_data;
    size_t serialized_data_len;
    serialized_data = data_packet_serialize(dp, &serialized_data_len);
    if (sendto (multicast_socket, serialized_data, serialized_data_len, 0,
                (struct sockaddr *) &multicast_addr,  sizeof(multicast_addr)) < 0) {
        critical_err("sendto failed");
    }
    free(serialized_data);
 
}


void* resend_requested_packets(void *arguments) {
    struct resend_queue_node *current;
    struct data_packet* requested_data;
    struct resend_queue* tmp;
    while (!finish) {
        // We need to lose volatile.
        tmp = (struct resend_queue*)collecting_queue;
        resend_queue_lock(tmp);
        collecting_queue = sending_queue;
        sending_queue = tmp;
        sending_queue->sending = true;
        // Same lock as previously.
        resend_queue_unlock(sending_queue);
        // We don't need lock here, because only this thread operates on
        // sending_queue.
        current = sending_queue->head;
        while (current != NULL) {
            data_queue_lock(queue);
            if(queue->tail == NULL ||
                    current->id  + queue->max_size / 2 > queue->tail->data_nr) {
                data_queue_unlock(queue);
                current = current->next;
                continue;
            }
            requested_data = data_queue_get_packet_by_id(queue, current->id);
            if (requested_data == NULL) {
                data_queue_unlock(queue);
                current = current->next;
                continue;
            }
            data_packet_inc_ref(requested_data);
            data_queue_unlock(queue);
            send_packet_on_multicast_addr(requested_data);
            data_packet_dec_ref(requested_data);
            current = current->next;
        }
        resend_queue_clear(sending_queue);
        usleep(RTIME * 1000);
    }
    return NULL;
}

int create_resend_thread(pthread_t* resend_thread) {
    return pthread_create(resend_thread, NULL, resend_requested_packets, NULL);
}

void add_packet_to_resend(unsigned int id) {
    // collecting_queue is volatile, used function returns false if
    // collecting_queue is currently being swaped with sending_queue.
    while(resend_queue_add_with_lock((struct resend_queue*)collecting_queue, id) != 0);
}

void* listen_to_control(void *arguments) {
    struct sockaddr_in addr;
    struct sockaddr_in client;
    struct control_packet *request;
    char control_packet_buffer[CTRL_PACKET_MAX_SERIALIZED_SIZE];
    socklen_t client_len = sizeof(client);

    int sd;

    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sd < 0) {
        critical_err("socked failed");
    }

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(CTRL_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0)
        critical_err("bind failed");

    while (!finish) {
        if (recvfrom(sd, control_packet_buffer, CTRL_PACKET_MAX_SERIALIZED_SIZE, 0,
                    (struct sockaddr*)&client, &client_len) < 0)
            critical_err("recvfrom failed");
        request = control_packet_deserialize(control_packet_buffer);
        switch(request->type) {
            case CTRL_PACKET_ID_REQUEST:
                if (sendto (sd, serialized_info, serialized_info_len, 0,
                            (struct sockaddr *) &client,  client_len) < 0) {
                    critical_err("sendto");
                }
                break;
            case CTRL_PACKET_RESEND_REQUEST:
                add_packet_to_resend(request->packet_id);
                break;
        }
        free(request);
    }
    close(sd);
    return NULL;
}

int create_control_thread(pthread_t* control_thread) {
    return pthread_create(control_thread, NULL, listen_to_control, NULL);
}

void init_nadajnik_info() {
    info = (struct nadajnik_info*)malloc(sizeof(struct nadajnik_info));
    info->application_name = APP_NAME;
    info->name = NAME;
    info->mcast_addr = MCAST_ADDR;
    info->version = VERSION;
    info->psize = PSIZE;
    serialized_info = nadajnik_info_serialize(info, &serialized_info_len);
}

int open_multicast_socket (int *sd, struct sockaddr_in* multi_addr) {
    int optval = 1;
    struct sockaddr_in local_addr;

    *sd = socket(PF_INET, SOCK_DGRAM, 0);
    if (*sd < 0)
        critical_err("socket failed");

    bzero(&local_addr, sizeof(local_addr));
    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    local_addr.sin_port = htons(0);

    if (bind(*sd, (struct sockaddr*)&local_addr, sizeof(local_addr)) != 0)
        critical_err("bind failed");

    optval = 1;
    if (setsockopt(*sd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) != 0)
        critical_err("Can't reuse address/ports");

    bzero(multi_addr, sizeof(struct sockaddr_in));
    multi_addr->sin_family = AF_INET;
    multi_addr->sin_port = htons(DATA_PORT);

    optval = 1;
    if (setsockopt(*sd, SOL_SOCKET, SO_BROADCAST, (void*)&optval, sizeof optval) < 0)
        critical_err("setsockopt broadcast");

    optval = TTL_VALUE;
    if (setsockopt(*sd, SOL_IP, IP_MULTICAST_TTL, (void*)&optval, sizeof optval) < 0)
        critical_err("setsockopt multicast ttl");

    if (inet_aton(MCAST_ADDR, &multi_addr->sin_addr) == 0)
        critical_err("inet_aton failed (%s)", MCAST_ADDR);
    return 0;
}

void init_global_variables(int argc, char **argv) {
    parse_command_line(argc, argv);
    QSIZE = FSIZE / PSIZE;
    init_nadajnik_info();
    queue = (struct data_queue*) malloc(sizeof(struct data_queue));
    data_queue_init(queue, QSIZE);
    sending_queue = (struct resend_queue*) malloc(sizeof(struct resend_queue));
    resend_queue_init(sending_queue);
    collecting_queue = (struct resend_queue*) malloc(sizeof(struct resend_queue));
    resend_queue_init((struct resend_queue*)collecting_queue);
    open_multicast_socket(&multicast_socket, &multicast_addr);
}

void destroy_global_variables() {
    data_queue_destroy(queue);
    resend_queue_clear(sending_queue);
    free(sending_queue);
    resend_queue_clear((struct resend_queue*)collecting_queue);
    free((struct resend_queue*)collecting_queue);
    close(multicast_socket);
    nadajnik_info_destroy(info);
    free(serialized_info);
}

int main (int argc, char **argv) {

    pthread_t control_thread;
    pthread_t resend_thread;

    char* buffer;
    char* current_buffer_ptr;
    size_t bytes_to_read = PSIZE;
    size_t current_size;
    int rb;
 
    unsigned int data_nr;
    struct data_packet* dp;

    signal(SIGTERM, sig_handler);
    init_global_variables(argc, argv);

    /* Creating thread listening to control requests. */
    create_control_thread(&control_thread);

    /* Create thread resending packets requested by one of odbiorniks. */
    create_resend_thread(&resend_thread);

    data_nr = 0;
    buffer = (char*) calloc(PSIZE, sizeof(char));
    while(!finish) {
        current_buffer_ptr = buffer;
        bytes_to_read = PSIZE;
        current_size = 0;
        while(!finish && bytes_to_read > 0) {
            rb = read(fileno(stdin), current_buffer_ptr, bytes_to_read);
            if (rb < 0) {
                critical_err("Reading from stdin failed");
            } else if (rb == 0) {
                finish = true;
            } else {
                current_buffer_ptr += rb;
                bytes_to_read -= rb;
                current_size += rb;
            }
        }
        dp = (struct data_packet*)malloc(sizeof(struct data_packet));
        data_packet_init(dp, current_size, data_nr, buffer);
        send_packet_on_multicast_addr(dp);
        data_queue_lock(queue);
        data_queue_push(queue, dp);
        data_queue_unlock(queue);
        data_packet_dec_ref(dp);
        data_nr++;
    }

    pthread_join(control_thread, NULL);
    pthread_join(resend_thread, NULL);
    destroy_global_variables();
    return 0;
}
