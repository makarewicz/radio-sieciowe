#include "resend_queue.h"

void resend_queue_init(struct resend_queue* q) {
    q->head = NULL;
    q->tail = NULL;
    pthread_mutex_init(&q->mutex, NULL);
    q->sending = false;
}

void resend_queue_clear(struct resend_queue* q) {
    struct resend_queue_node *current, *next;
    current = q->head;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
    resend_queue_init(q);
}

void resend_queue_add(struct resend_queue* q, unsigned int id) {
    if (q->tail == NULL) {
        q->head = q->tail = (struct resend_queue_node*)malloc(sizeof(struct resend_queue_node));
    } else {
        q->tail->next = (struct resend_queue_node*)malloc(sizeof(struct resend_queue_node));
        q->tail = q->tail->next;
    }
    q->tail->next = NULL;
    q->tail->id = id;
}

int resend_queue_add_with_lock(struct resend_queue* q, unsigned int id) {
    resend_queue_lock(q);
    if(q->sending) {
        resend_queue_unlock(q);
        return -1;
    }
    resend_queue_add(q, id);
    resend_queue_unlock(q);
    return 0;
}

void resend_queue_lock(struct resend_queue* q) {
    pthread_mutex_lock(&q->mutex);
}

void resend_queue_unlock(struct resend_queue* q) {
    pthread_mutex_unlock(&q->mutex);
}
