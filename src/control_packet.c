#include "control_packet.h"
#define ITOA_BUF_SIZE 20

char* control_packet_serialize(struct control_packet* cp, size_t *result_len) {
    char *type_str, *packet_id_str, *result, *current_ptr;
    size_t type_len, packet_id_len;
    
    type_str = (char*)calloc(ITOA_BUF_SIZE, sizeof(char));
    sprintf(type_str, "%d", cp->type);
    type_len = strlen(type_str);

    packet_id_str = (char*)calloc(ITOA_BUF_SIZE, sizeof(char));
    sprintf(packet_id_str, "%d", cp->packet_id);
    packet_id_len = strlen(packet_id_str);

    *result_len = type_len + packet_id_len + 2;

    result = (char*)calloc(*result_len, sizeof(char));
    current_ptr = result;

    strcpy(current_ptr, type_str);
    current_ptr += type_len + 1;
    free(type_str);

    strcpy(current_ptr, packet_id_str);
    current_ptr += packet_id_len + 1;
    free(packet_id_str);

    return result;
}

struct control_packet* control_packet_deserialize(char* cp) {
    char* current_ptr;
    struct control_packet* result;

    result = (struct control_packet*)malloc(sizeof(struct control_packet));
    current_ptr = cp;

    result->type = atoi(current_ptr);
    current_ptr += strlen(current_ptr) + 1;

    result->packet_id = atoi(current_ptr);
    return result;
}
