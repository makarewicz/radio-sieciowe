#ifndef NADAJNIK_INFO_H
#define NADAJNIK_INFO_H

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#define NADAJNIK_INFO_MAX_SERIALIZED_SIZE 300

struct nadajnik_info {
    char* application_name;
    char* name;
    char* mcast_addr;
    char* version;
    size_t psize;
    // Member used only in odbiornik, remembers ipaddress of 
    unsigned long s_addr;
};

void nadajnik_info_destroy(struct nadajnik_info* info);
char* nadajnik_info_serialize(struct nadajnik_info* info, size_t* result_len);
struct nadajnik_info* nadajnik_info_deserialize(char* info);
int nadajnik_info_cmp(struct nadajnik_info* lhs, struct nadajnik_info* rhs);

#endif
