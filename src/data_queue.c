#include "data_queue.h"

void data_queue_init(struct data_queue* q, size_t max_size) {
    q->max_size = max_size;
    pthread_mutex_init(&q->mutex, NULL);
    q->packets = (struct data_packet**)calloc(max_size, sizeof(struct data_packet*));
    q->begin = q->packets;
    q->end = q->packets;
    q->head = NULL;
    q->tail = NULL;
    q->non_empty_elements = 0;
}

struct data_packet** data_queue_get_nth_packet_ptr(struct data_queue* q, size_t n) {
    struct data_packet** pos;
    if (n >= q->max_size) {
        return NULL;
    }
    pos = q->begin + n;
    if(pos >= q->packets + q->max_size) {
        pos -= q->max_size;
    }
    return pos;
}

struct data_packet* data_queue_get_nth_packet(struct data_queue* q, size_t n) {
    return *data_queue_get_nth_packet_ptr(q, n);
}

struct data_packet* data_queue_get_packet_by_id(struct data_queue* q, size_t id) {
    if(q->head == NULL)
        return NULL;
    if(id < q->head->data_nr || id > q->tail->data_nr)
        /* index out of range. */
        return NULL;
    return data_queue_get_nth_packet(q, id - q->head->data_nr);
}

void data_queue_destroy(struct data_queue* q) {
    for(int i = 0; i < q->max_size; ++i) {
        if(q->packets[i] != NULL)
            data_packet_dec_ref(q->packets[i]);
    }
    free(q->packets);
    pthread_mutex_destroy(&q->mutex);
    free(q);
}

void data_queue_set_packet(struct data_queue* q, struct data_packet** pos, struct data_packet* new_data) {
    if(*pos != NULL) {
        if((*pos)->data != NULL)
            q->non_empty_elements--;
        data_packet_dec_ref(*pos);
    }
    if (new_data != NULL) {
        if(new_data->data != NULL)
            q->non_empty_elements++;
        data_packet_inc_ref(new_data);
    }
    *pos = new_data;
}

void data_queue_set_nth_packet(struct data_queue* q, size_t n, struct data_packet* data) {
    struct data_packet** pos;
    pos = data_queue_get_nth_packet_ptr(q, n);
    data_queue_set_packet(q, pos, data);
}

void data_queue_set_packet_by_id(struct data_queue* q, struct data_packet* data) {
    struct data_packet* dummy_packet;
    if (q->head == NULL) {
        /* First packet in queue, always put on front */
        //fprintf(stderr, "queue is empty, push is enough.\n");
        data_queue_push(q, data);
        //fprintf(stderr, "finished with push.\n");
        return;
    }
    if (data->data_nr < q->head->data_nr) {
        /* We're not interested in packets before q->head. */
        return;
    }
    //fprintf(stderr, "something is in the queue, let's make compute new position.");
    while(data->data_nr > q->tail->data_nr) {
        dummy_packet = (struct data_packet*)malloc(sizeof(struct data_packet));
        data_packet_init(dummy_packet, 0, q->tail->data_nr + 1, NULL);
        /* Add missing packets with data NULL. */
        data_queue_push(q, dummy_packet);
    }
    data_queue_set_nth_packet(q, data->data_nr - q->head->data_nr, data);
    //fprintf(stderr, "setting up new packet finished.\n");
}

void data_queue_push(struct data_queue* q, struct data_packet* data) {
    //fprintf(stderr, "Setting packet at the end\n");
    data_queue_set_packet(q, q->end, data);
    //fprintf(stderr, "Set packet at the end\n");
    bool end_same_as_begin = false;
    if(q->head != NULL && q->end == q->begin)
        end_same_as_begin = true;
    //fprintf(stderr, "Changing tail\n");
    q->tail = *q->end;
    q->end++;
    //fprintf(stderr, "changing end (if needed)\n");
    if(q->end >= q->packets + q->max_size) {
        q->end = q->packets;
    }
    //fprintf(stderr, "changing begin (if needed)\n");
    if(end_same_as_begin) {
        //fprintf(stderr, "changing begin\n");
        q->begin = q->end;
    }
    //fprintf(stderr, "changing head\n");
    q->head = *q->begin;
    return;
}

struct data_packet* data_queue_pop(struct data_queue* q) {
    struct data_packet* dp;
    if (q->head == NULL) {
        return NULL;
    }
    dp = q->head;
    data_packet_inc_ref(dp);
    data_queue_set_packet(q, q->begin, NULL);
    q->begin++;
    if(q->begin >= q->packets + q->max_size) {
        q->begin = q->packets;
    }
 
    if(q->begin == q->end) {
        q->head = NULL;
        q->tail = NULL;
    } else {
        q->head = *q->begin;
    }
    return dp;
}

void data_queue_lock(struct data_queue* q) {
    pthread_mutex_lock(&q->mutex);
}

void data_queue_unlock(struct data_queue* q) {
    pthread_mutex_unlock(&q->mutex);
}

bool data_queue_is_buffered(struct data_queue* q) {
    return q->non_empty_elements * 4 >= q->max_size * 3;
}
