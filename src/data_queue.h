#ifndef DATA_QUEUE_H
#define DATA_QUEUE_H

#include "data_packet.h"
#include <pthread.h>

struct data_queue {
    size_t max_size;
    int non_empty_elements;
    pthread_mutex_t mutex;
    struct data_packet** packets;
    struct data_packet** begin;
    struct data_packet** end;
    struct data_packet* head;
    struct data_packet* tail;
};

void data_queue_init(struct data_queue* q, size_t max_size);
struct data_packet** data_queue_get_nth_packet_ptr(struct data_queue* q, size_t n);
struct data_packet* data_queue_get_nth_packet(struct data_queue* q, size_t n);
struct data_packet* data_queue_get_packet_by_id(struct data_queue* q, size_t id);
void data_queue_destroy(struct data_queue* q);
void data_queue_set_packet(struct data_queue* q, struct data_packet** pos, struct data_packet* new_data);
void data_queue_set_packet_by_id(struct data_queue* q, struct data_packet* data);
void data_queue_set_nth_packet(struct data_queue* q, size_t n, struct data_packet* data);
void data_queue_push(struct data_queue* q, struct data_packet* data);
void data_queue_lock(struct data_queue* q);
void data_queue_unlock(struct data_queue* q);
struct data_packet* data_queue_pop(struct data_queue* q);
bool data_queue_is_buffered(struct data_queue* q);

#endif
