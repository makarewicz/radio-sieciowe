#include "util.h"

void critical_err(char* msg, ...) {
    va_list ap;
	va_start(ap, msg);
	vprintf(msg, ap);
	perror("errno");
	va_end(ap);
	abort();
}
