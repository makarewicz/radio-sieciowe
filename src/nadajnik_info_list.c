#include "nadajnik_info_list.h"

void nadajnik_info_list_init(struct nadajnik_info_list* list) {
    list->current_size = 0;
    list->max_size = 1;
    list->info = calloc(1, sizeof(struct nadajnik_info*));
    list->request_cnt = calloc(1, sizeof(size_t));
    pthread_mutex_init(&list->mutex, NULL);
}

void nadajnik_info_list_destroy(struct nadajnik_info_list* list) {
    size_t i;
    for(i = 0; i < list->current_size; ++i) {
        nadajnik_info_destroy(list->info[i]);
    }
    free(list->info);
    free(list->request_cnt);
    pthread_mutex_destroy(&list->mutex);
}

void nadajnik_info_list_lock(struct nadajnik_info_list* list) {
    pthread_mutex_lock(&list->mutex);
}

void nadajnik_info_list_unlock(struct nadajnik_info_list* list) {
    pthread_mutex_unlock(&list->mutex);
}

void nadajnik_info_list_add_nadajnik(
        struct nadajnik_info_list* list, struct nadajnik_info* info) {
    if(list->current_size == list->max_size) {
        list->max_size *= 2;
        list->info = realloc(list->info, list->max_size * sizeof(struct nadajnik_info*));
        list->request_cnt = realloc(list->request_cnt, list->max_size * sizeof(size_t));
    }
    list->info[list->current_size] = info;
    list->request_cnt[list->current_size] = 0;
    list->current_size++;
}

int nadajnik_info_list_remove(struct nadajnik_info_list* list, size_t index) {
    if(index >= list->current_size) {
        return -1;
    }
    nadajnik_info_destroy(list->info[index]);
    list->current_size--;
    memmove(list->info + index, list->info + index + 1,
            (list->current_size - index) * sizeof(struct nadajnik_info*));
    memmove(list->request_cnt + index, list->request_cnt + index + 1,
            (list->current_size - index) * sizeof(size_t));
    return 0;
}

int nadajnik_info_list_match_info(struct nadajnik_info_list* list, struct nadajnik_info* info) {
    size_t i;
    
    for(i = 0; i < list->current_size; ++i) {
        if (nadajnik_info_cmp(info, list->info[i]) == 0) {
            return i;
        }
    }
    return -1;
}
