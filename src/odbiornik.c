#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>
#include <pthread.h>
#include <limits.h>
#include <poll.h>
#include <signal.h>
#include "data_packet.h"
#include "data_queue.h"
#include "control_packet.h"
#include "util.h"
#include "resend_queue.h"
#include "nadajnik_info.h"
#include "nadajnik_info_list.h"

#define MAX_REQUEST_CNT 4
#define TTL_VALUE 2
#define UI_BUF_SIZE 1024

/* Parametry konfiguracyjne: */
/* Adres rozgłaszania ukierunkowanego */
static char* DISCOVER_ADDR = "255.255.255.255";
/* Port UDP używany do przesyłania danych. */
static int DATA_PORT = 26310;
/* Port UDP używany do transmisji pakietów kontrolnych */
static int CTRL_PORT = 36310;
/* Port TCP, na ktorym udostepniany jest prosty interfejs tekstowy
 * do przelaczania sie miedzy stacjami. */
static int UI_PORT = 16310;
/* Rozmiar w bajtach bufora. */
static size_t BSIZE = 65536;
/* Rozmiar w bajtach pakietu danych otrzymywanych z aktualnej stacji. */
static size_t PSIZE;
/* Liczba pakietow trzymanych w buforze. */
static size_t QSIZE;
/* Czas (w milisekundach) między kolejnymi retransmisjami paczek. */
static int RTIME = 100;
/* Application name */
static char* APP_NAME;
/* Preferred station */
static char* PREFFERED_STATION_NAME;

void refresh_menu();

volatile static bool finish = false;
static struct data_queue* queue;

static struct pollfd client[_POSIX_OPEN_MAX];
static struct nadajnik_info_list infos;
static struct nadajnik_info* current_nadajnik = NULL;
static pthread_mutex_t current_nadajnik_mutex;
static pthread_cond_t nadajnik_is_buffered;
static pthread_mutex_t buffer_mutex;

void odbiornik_wait_for_buffer() {
    pthread_mutex_lock(&buffer_mutex);
    pthread_cond_wait(&nadajnik_is_buffered, &buffer_mutex);
    pthread_mutex_unlock(&buffer_mutex);
}

void odbiornik_signal_nadajnik_is_buffered() {
    pthread_mutex_lock(&buffer_mutex);
    pthread_cond_broadcast(&nadajnik_is_buffered);
    pthread_mutex_unlock(&buffer_mutex);
}

void sig_handler(int sig) {
    switch (sig) {
        case SIGTERM:
            finish = true;
        default:
            fprintf(stderr, "wasn't expecting that!\n");
            abort();
    }
}


int odbiornik_parse_command_line (int argc, char** argv) {
    int c;
    APP_NAME = argv[0];
    while ((c = getopt (argc, argv, "d:P:C:U:b:R:n:")) != -1)
        switch (c) {
            case 'd':
                DISCOVER_ADDR = optarg;
                break;
            case 'P':
                DATA_PORT = atoi(optarg);
                break;
            case 'C':
                CTRL_PORT = atoi(optarg);
                break;
            case 'U':
                UI_PORT = atoi(optarg);
                break;
            case 'b':
                BSIZE = atoi(optarg);
                break;
            case 'R':
                RTIME = atoi(optarg);
                break;
            case 'n':
                PREFFERED_STATION_NAME = optarg;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                critical_err("Parsing command line failed");
                return 1;
        }
    return 0;
}

void odbiornik_change_current_nadajnik(struct nadajnik_info* info) {
    fprintf(stderr, "changing current station\n");
    // After first change we don't care about preffered station.
    PREFFERED_STATION_NAME = NULL;
    if(current_nadajnik != NULL) {
        pthread_mutex_lock(&current_nadajnik_mutex);
        data_queue_destroy(queue);
    }
    current_nadajnik = info;
    if (current_nadajnik != NULL) {
        PSIZE = current_nadajnik->psize;
        fprintf(stderr, "info: app_name %s, name %s,\nmcast_addr %s, version %s, psize %d\n",
            info->application_name, info->name, info->mcast_addr, info->version, (int)info->psize);
        QSIZE = BSIZE / PSIZE;
        queue = (struct data_queue*) malloc(sizeof(struct data_queue));
        data_queue_init(queue, QSIZE); 
        pthread_mutex_unlock(&current_nadajnik_mutex);
    }
    refresh_menu();
}

void odbiornik_play_diffrent_nadajnik(int diff) {
    int i;
    nadajnik_info_list_lock(&infos);
    if (infos.current_size == 0) {
        nadajnik_info_list_unlock(&infos);
        return;
    }
    if(current_nadajnik == NULL) {
        nadajnik_info_list_unlock(&infos);
        odbiornik_change_current_nadajnik(infos.info[0]);
    } else {
        for(i = 0; i < infos.current_size; ++i) {
            if(nadajnik_info_cmp(current_nadajnik, infos.info[i]) == 0) {
                nadajnik_info_list_unlock(&infos);
                odbiornik_change_current_nadajnik(
                        infos.info[((i + infos.current_size)+ diff) % infos.current_size]);
                break;
            }
        }
    }
    nadajnik_info_list_unlock(&infos);

}

void odbiornik_play_next_nadajnik(void) {
    odbiornik_play_diffrent_nadajnik(1);
}

void odbiornik_play_prev_nadajnik(void) {
    odbiornik_play_diffrent_nadajnik(-1);
}

void* odbiornik_discover_control(void *arguments) {
    int sd = *(int*)arguments;
    free(arguments);


    /* Counter for initial fast pinging. */
    int initial_counter = 6;

    struct sockaddr_in discover_addr;
    struct control_packet cp;
    char* cp_serialized;
    size_t cp_serialized_len;
    size_t i;
    int r;
    int optval;

    cp.type = CTRL_PACKET_ID_REQUEST;
    cp_serialized = control_packet_serialize(&cp, &cp_serialized_len);
    optval = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (void*)&optval, sizeof optval) < 0)
        critical_err("setsockopt broadcast");

    optval = TTL_VALUE;
    if (setsockopt(sd, SOL_IP, IP_MULTICAST_TTL, (void*)&optval, sizeof optval) < 0)
        critical_err("setsockopt multicast ttl");

    discover_addr.sin_family = AF_INET;
    discover_addr.sin_port = htons(CTRL_PORT);

    if (inet_aton(DISCOVER_ADDR, &discover_addr.sin_addr) == 0)
        critical_err("inet_aton failed (%s)", DISCOVER_ADDR);

    while (!finish) {
        if (sendto(sd, cp_serialized, cp_serialized_len, 0,
                    (struct sockaddr*) &discover_addr, sizeof(discover_addr)) < 0) {
            critical_err("sendto failed");
        }
        for(i = 0; i < infos.current_size; ++i) {
            r = __sync_fetch_and_add(&(infos.request_cnt[i]), 1);
            if(r > MAX_REQUEST_CNT) {
                nadajnik_info_list_lock(&infos);
                if(nadajnik_info_cmp(current_nadajnik, infos.info[i]) == 0) {
                    odbiornik_change_current_nadajnik(NULL);
                }
                nadajnik_info_list_remove(&infos, i);
                nadajnik_info_list_unlock(&infos);
                refresh_menu();
                /* New nadajnik_info is at current i. */
                --i;
            }
        }
        if(initial_counter > 0) {
            usleep(500 * 1000);
            initial_counter--;
        } else {
            usleep(5000 * 1000);
        }
    }

    close(sd);
    free(cp_serialized);
    return 0;
}

void* odbiornik_listen_to_control(void *arguments) {
    int sd = *(int*)arguments;
    free(arguments);
    struct sockaddr_in server;
    struct nadajnik_info *info;
    char nadajnik_info_buffer[NADAJNIK_INFO_MAX_SERIALIZED_SIZE];
    int r;
    socklen_t server_len = sizeof(server);
    while (!finish) {
        if (recvfrom(sd, nadajnik_info_buffer, NADAJNIK_INFO_MAX_SERIALIZED_SIZE, 0,
                    (struct sockaddr*)&server, &server_len) < 0)
            critical_err("recvfrom failed");
        info = nadajnik_info_deserialize(nadajnik_info_buffer);
        nadajnik_info_list_lock(&infos);
        r = nadajnik_info_list_match_info(&infos, info);
        if(r >= 0) {
            infos.request_cnt[r] = 0;
            nadajnik_info_list_unlock(&infos);
            nadajnik_info_destroy(info);
        } else {
            info->s_addr = server.sin_addr.s_addr;
            nadajnik_info_list_add_nadajnik(&infos, info);
            nadajnik_info_list_unlock(&infos);
            if(current_nadajnik == NULL) {
                if(PREFFERED_STATION_NAME == NULL ||
                        strcmp(info->name, PREFFERED_STATION_NAME) == 0)
                    odbiornik_change_current_nadajnik(info);
            }
            refresh_menu();
        }
    }
    close(sd);
    return NULL;
}

void* odbiornik_resend_request(void *arguments) {
    int sd;
    struct sockaddr_in local_address;
    struct sockaddr_in server_address;
    unsigned int last_packet = 0;
    unsigned int i;
    struct nadajnik_info* last_nadajnik = NULL;
    char* serialized_request;
    struct control_packet request;
    size_t serialized_request_len;
    struct data_packet* dp;
    request.type = CTRL_PACKET_RESEND_REQUEST;
    int optval;

    sd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sd < 0)
        critical_err("socket failed.");

    bzero(&local_address, sizeof(local_address));
    local_address.sin_family = AF_INET;
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    local_address.sin_port = htons(0);

    if (bind(sd, (struct sockaddr*) &local_address, sizeof(local_address)) != 0)
        critical_err("bind failed.");

    optval = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) != 0)
        critical_err("Can't reuse address/ports");

    while(!finish) {
        pthread_mutex_lock(&current_nadajnik_mutex);
        if(current_nadajnik != last_nadajnik) {
            if(current_nadajnik != NULL) {
                server_address.sin_family = AF_INET;
                server_address.sin_addr.s_addr = current_nadajnik->s_addr;
                server_address.sin_port = htons(CTRL_PORT);
                last_packet = 0;
            }
            last_nadajnik = current_nadajnik;
        }
        if(current_nadajnik != NULL) {
            /* Iterating through bufor of packets, requesting the one we don't have. */
            data_queue_lock(queue);
            dp = queue->head;
            if(dp == NULL) {
                pthread_mutex_unlock(&current_nadajnik_mutex);
                data_queue_unlock(queue);
                usleep(RTIME * 1000);
                continue;
            }
            data_packet_inc_ref(dp);
            data_queue_unlock(queue);
            i = dp->data_nr;
            data_packet_dec_ref(dp);
            for(; i < last_packet; ++i) {
                data_queue_lock(queue);
                dp = data_queue_get_packet_by_id(queue, i);
                if(dp == NULL) {
                    data_queue_unlock(queue);
                    continue;
                }
                data_packet_inc_ref(dp);
                data_queue_unlock(queue);
                if (dp->data == NULL) {
                    request.packet_id = dp->data_nr;
                    serialized_request = control_packet_serialize(&request, &serialized_request_len);
                    sendto(sd, serialized_request, serialized_request_len, 0,
                            (struct sockaddr *) &server_address, sizeof(server_address));
                    free(serialized_request);
                }
                data_packet_dec_ref(dp);
            }
            data_queue_lock(queue);
            if(queue->tail != NULL)
                last_packet = queue->tail->data_nr;
            data_queue_unlock(queue);
        }
        pthread_mutex_unlock(&current_nadajnik_mutex);
        usleep(RTIME * 1000);
    }
    close(sd);
    return NULL;
}

int odbiornik_create_control_thread(
        pthread_t* listen_control_thread,
        pthread_t* discover_thread,
        pthread_t* resend_requests_thread) {
    struct sockaddr_in local_address;
    int sd;
    int r;
    int optval;
    int* arg;
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sd < 0) {
        critical_err("socked failed");
    }

    bzero(&local_address, sizeof(local_address));
    local_address.sin_family = AF_INET;
    local_address.sin_port = htons(0);
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr*)&local_address, sizeof(local_address)) != 0)
        critical_err("bind failed");

    optval = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) != 0)
        critical_err("Can't reuse address/ports");

    arg = (int*)malloc(sizeof(int));
    *arg = sd;
    r = pthread_create(listen_control_thread, NULL, odbiornik_listen_to_control, (void*)arg);

    if (r != 0) {
        critical_err("pthread_create failed.");
        return r;
    }

    arg = (int*)malloc(sizeof(int));
    *arg = sd;
    r = pthread_create(discover_thread, NULL, odbiornik_discover_control, (void*)arg);

    if (r != 0) {
        critical_err("pthread_create failed.");
        return r;
    }
    r = pthread_create(resend_requests_thread, NULL, odbiornik_resend_request, NULL);
    if (r != 0) {
        critical_err("pthread_create failed.");
        return r;
    }
    return 0;
}

void* odbiornik_listen_to_current_station(void* arguments) {
    int sd;
    struct sockaddr_in local_address;
    struct sockaddr_in server_address;
    struct nadajnik_info* last_nadajnik = NULL;
    struct data_packet* dp;
    struct ip_mreq mreq;
    char data_packet_buffer[DATA_PACKET_MAX_SERIALIZED_SIZE];
    socklen_t server_len = sizeof(server_address);

    sd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sd < 0)
        critical_err("socket failed.");

    bzero(&local_address, sizeof(local_address));
    local_address.sin_family = AF_INET;
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    local_address.sin_port = htons(DATA_PORT);

    if (bind(sd, (struct sockaddr*) &local_address, sizeof(local_address)) != 0)
        critical_err("bind failed.");

    while (!finish) {
        pthread_mutex_lock(&current_nadajnik_mutex);
        if (current_nadajnik != last_nadajnik) {
            if(last_nadajnik != NULL)
                if (setsockopt(sd, SOL_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) != 0)
                    critical_err("Droping multicast membership failed");
            if(current_nadajnik != NULL) {
                mreq.imr_interface.s_addr = INADDR_ANY;
                if (current_nadajnik != NULL) {
                    if (inet_aton(current_nadajnik->mcast_addr, &mreq.imr_multiaddr) == 0)
                        critical_err("address (%s) bad", current_nadajnik->mcast_addr);
                }
                if (setsockopt(sd, SOL_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) != 0)
                    critical_err("Join multicast failed");
            }
            last_nadajnik = current_nadajnik;
        }
        pthread_mutex_unlock(&current_nadajnik_mutex);
        if (recvfrom(sd, data_packet_buffer, DATA_PACKET_MAX_SERIALIZED_SIZE, 0,
                    (struct sockaddr*)&server_address, &server_len) < 0)
            critical_err("recvfrom failed.");
        dp = data_packet_deserialize(data_packet_buffer);
        pthread_mutex_lock(&current_nadajnik_mutex);
        if(current_nadajnik->s_addr == server_address.sin_addr.s_addr) {
            data_queue_lock(queue);
            data_queue_set_packet_by_id(queue, dp);
            
            if(data_queue_is_buffered(queue)) {
                odbiornik_signal_nadajnik_is_buffered();
            }
           
            data_queue_unlock(queue);
        }
        pthread_mutex_unlock(&current_nadajnik_mutex);
        data_packet_dec_ref(dp);
    }
    close(sd);
    return NULL;
}

int odbiornik_create_listen_to_station_thread(pthread_t* listen_to_station_thread) {
    int r;
    r = pthread_create(listen_to_station_thread, NULL, odbiornik_listen_to_current_station, NULL);
    if (r != 0) {
        critical_err("pthread_create failed");
        return r;
    }
    return 0;
}

void refresh_menu() {
    size_t i, cur_line_width;
    size_t r;
    char buf[UI_BUF_SIZE];
    int line_width = 63;
    char *current_ptr;
    char move_to_newline_buf[] = "\033[1B\033[63D";
    int fd;

    sprintf(buf,"%s%s%s%s%s%s%s%s",
            "+-------------------------------------------------------------+",
            move_to_newline_buf,
            "| Odbiornik elektorniczno-internetowych fal radiowych         |",
            move_to_newline_buf,
            "+-------------------------------------------------------------+",
            move_to_newline_buf,
            "| Dostepne stacje:                                            |",
            move_to_newline_buf);

    current_ptr = buf + strlen(buf);
    for(i = 0; i < infos.current_size; ++i) {
        cur_line_width = 0;
        //nadajnik_info_list_lock(&infos);
        
        sprintf(current_ptr, "|  ");
        r = strlen(current_ptr);
        cur_line_width += r;
        current_ptr += r;
        
        if(nadajnik_info_cmp(current_nadajnik, infos.info[i]) == 0) {
            sprintf(current_ptr, ">");
        } else {
            sprintf(current_ptr, " ");
        }
        r = strlen(current_ptr);
        cur_line_width += r;
        current_ptr += r;

        sprintf(current_ptr, " %d. ", (int)i + 1);
        r = strlen(current_ptr);
        cur_line_width += r;
        current_ptr += r;

        sprintf(current_ptr, "%-*s|%s",
                (int)line_width - (int)cur_line_width - 1,
                infos.info[i]->name, move_to_newline_buf);
        current_ptr += strlen(current_ptr);
        //nadajnik_info_list_unlock(&infos);
    }

    sprintf(current_ptr,
            "+-------------------------------------------------------------+");
    for (i = 1; i < _POSIX_OPEN_MAX; ++i) {
        fd = client[i].fd;
        if (fd != -1) {
            // Clear terminal.
            write(fd, "\033[2J", 4 );
            // Move cursor to upper right corner.
            write(fd, "\033[H", 3);	
            write(fd, buf, strlen(buf));
        }
    }
}

void* odbiornik_ui_func(void* arguments) {
    struct sockaddr_in server;
    unsigned char buf[UI_BUF_SIZE];
    size_t length;
    ssize_t rval;
    int msgsock, activeClients, i, ret;
    int optval;
    int j;

    char echo_mode_command[] = {
        (char)255, (char)253, (char)34, /* IAC DO LINEMODE */
        (char)255, (char)250, (char)34, (char)1, (char)0, (char)255, (char)240, /* IAC SB LINEMODE MODE 0 IAC SE */
        (char)255, (char)251, (char)1 /* IAC WILL ECHO */
    };
    size_t echo_mode_command_len = sizeof(echo_mode_command);

    /* Inicjujemy tablicÄ™ z gniazdkami klientÃ³w, client[0] to gniazdko centrali */
    for (i = 0; i < _POSIX_OPEN_MAX; ++i) {
        client[i].fd = -1;
        client[i].events = POLLIN;
        client[i].revents = 0;
    }
    activeClients = 0;
    client[0].fd = socket(PF_INET, SOCK_STREAM, 0);
    if (client[0].fd < 0) {
        critical_err("Opening stream socket");
    }

    optval = 1;
    if (setsockopt(client[0].fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) != 0)
        critical_err("Can't reuse address/ports");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(UI_PORT);
    if (bind(client[0].fd, (struct sockaddr*)&server,
                (socklen_t)sizeof(server)) < 0) {
        critical_err("Binding stream socket");
    }

    length = sizeof(server);
    if (getsockname (client[0].fd, (struct sockaddr*)&server,
                (socklen_t*)&length) < 0) {
        critical_err("Getting socket name");
    }

    if (listen(client[0].fd, 5) == -1) {
        critical_err("Starting to listen");
    }

    /* Do pracy */
    do {
        for (i = 0; i < _POSIX_OPEN_MAX; ++i)
            client[i].revents = 0;
        if (finish && client[0].fd >= 0) {
            if (close(client[0].fd) < 0)
                critical_err("close");
            client[0].fd = -1;
        }

        /* Czekamy przez 5000 ms */
        ret = poll(client, _POSIX_OPEN_MAX, 5000);
        if (ret < 0)
            critical_err("poll");
        else if (ret > 0) {
            if (!finish && (client[0].revents & POLLIN)) {
                msgsock =
                    accept(client[0].fd, (struct sockaddr*)0, (socklen_t*)0);
                if (msgsock == -1)
                    critical_err("accept");
                else {
                    for (i = 1; i < _POSIX_OPEN_MAX; ++i) {
                        if (client[i].fd == -1) {
                            client[i].fd = msgsock;
                            activeClients += 1;
                            // Telnet negotiations:
                            write(client[i].fd, echo_mode_command, echo_mode_command_len);
                            refresh_menu();
                            break;
                        }
                    }
                    if (i >= _POSIX_OPEN_MAX) {
                        fprintf(stderr, "Too many clients\n");
                        if (close(msgsock) < 0)
                            critical_err("close");
                    }
                }
            }
            for (i = 1; i < _POSIX_OPEN_MAX; ++i) {
                if (client[i].fd != -1
                        && (client[i].revents & (POLLIN | POLLERR))) {
                    rval = read(client[i].fd, buf, UI_BUF_SIZE);
                    if (rval < 0) {
                        critical_err("Reading stream message");
                        if (close(client[i].fd) < 0)
                            perror("close");
                        client[i].fd = -1;
                        activeClients -= 1;
                    }
                    else if (rval == 0) {
                        if (close(client[i].fd) < 0)
                            critical_err("close");
                        client[i].fd = -1;
                        activeClients -= 1;
                    }
                    else {
                        for(j = 0; j < rval; j += 3) {
                            if(buf[j] != '\033' || buf[j + 1] != '[')
                                continue;
                            if(buf[j + 2] == 'A')
                                odbiornik_play_prev_nadajnik();
                            else if(buf[j + 2] == 'B')
                                odbiornik_play_next_nadajnik();
                        }
                    }
                }
            }
        }
    } while (!finish);

    if (client[0].fd >= 0)
        if (close(client[0].fd) < 0)
            perror("Closing main socket");
    return NULL;
}

int odbiornik_create_ui_thread(pthread_t* ui_thread) {
    int r;
    r = pthread_create(ui_thread, 0, odbiornik_ui_func, NULL);
    if (r != 0) {
        critical_err("pthread_create failed");
        return r;
    }
    return 0;
}

void odbiornik_init_global_variables(int argc, char **argv) {
    odbiornik_parse_command_line(argc, argv);
    nadajnik_info_list_init(&infos);
    pthread_mutex_init(&current_nadajnik_mutex, 0);
    pthread_mutex_lock(&current_nadajnik_mutex);
    pthread_cond_init(&nadajnik_is_buffered, 0);
    pthread_mutex_init(&buffer_mutex, 0);
}

void odbiornik_destroy_global_variables() {
    nadajnik_info_list_destroy(&infos);
    pthread_mutex_destroy(&current_nadajnik_mutex);
    pthread_mutex_destroy(&buffer_mutex);
    pthread_cond_destroy(&nadajnik_is_buffered);
}

int main (int argc, char **argv) {

    pthread_t discover_thread;
    pthread_t listen_control_thread;
    pthread_t resend_requests_thread;
    pthread_t listen_to_station_thread;
    pthread_t ui_thread;

    struct data_packet* dp;

    signal(SIGTERM, sig_handler);
    odbiornik_init_global_variables(argc, argv);

    odbiornik_create_control_thread(&listen_control_thread, &discover_thread,
            &resend_requests_thread);

    odbiornik_create_listen_to_station_thread(&listen_to_station_thread);
    odbiornik_create_ui_thread(&ui_thread);

    while(!finish) {
        pthread_mutex_lock(&current_nadajnik_mutex);
        data_queue_lock(queue);
        dp = data_queue_pop(queue);
        data_queue_unlock(queue);
        pthread_mutex_unlock(&current_nadajnik_mutex);
        if(dp == NULL || dp->data == NULL) {
            odbiornik_wait_for_buffer();
            continue;
        }
        write(fileno(stdout), dp->data, dp->data_size);
    }
    pthread_join(listen_control_thread, NULL);
    pthread_join(discover_thread, NULL);
    pthread_join(resend_requests_thread, NULL);
    pthread_join(listen_to_station_thread, NULL);
    pthread_join(ui_thread, NULL);
    odbiornik_destroy_global_variables();
    return 0;
}
