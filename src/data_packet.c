#include "data_packet.h"
#define ITOA_BUF_SIZE 20

void data_packet_init(
        struct data_packet* dp, size_t data_size, unsigned int data_nr, char* data) {
    dp->data_size = data_size;
    dp->data_nr = data_nr;
    if(data != NULL) {
        dp->data = (char*) calloc(data_size, sizeof(char));
        memcpy(dp->data, data, data_size);
    } else {
        dp->data = NULL;
    }
    dp->ref_cnt = 1;
}

void data_packet_destroy(struct data_packet* dp) {
    free(dp->data);
    free(dp);
}

void data_packet_inc_ref(struct data_packet* dp) {
    __sync_fetch_and_add(&(dp->ref_cnt), 1);
}

void data_packet_dec_ref(struct data_packet* dp) {
    int r = __sync_fetch_and_sub(&(dp->ref_cnt), 1);
    if(r == 1) {
        data_packet_destroy(dp);
    }
}

char* data_packet_serialize(struct data_packet* dp, size_t* result_len) {
    char *data_nr_str, *data_size_str, *result, *current_ptr;
    size_t data_size_len, data_nr_len;
    
    data_nr_str = (char*)calloc(ITOA_BUF_SIZE, sizeof(char));
    sprintf(data_nr_str, "%u", dp->data_nr);
    data_nr_len = strlen(data_nr_str);

    data_size_str = (char*)calloc(ITOA_BUF_SIZE, sizeof(char));
    sprintf(data_size_str, "%d", (int)dp->data_size);
    data_size_len = strlen(data_size_str);

    *result_len = data_nr_len + data_size_len + dp->data_size + 2;

    result = (char*)calloc(*result_len, sizeof(char));
    current_ptr = result;

    strcpy(current_ptr, data_nr_str);
    current_ptr += data_nr_len + 1;
    free(data_nr_str);

    strcpy(current_ptr, data_size_str);
    current_ptr += data_size_len + 1;
    free(data_size_str);

    memcpy(current_ptr, dp->data, dp->data_size);
    return result;
}

struct data_packet* data_packet_deserialize(char* dp) {
    struct data_packet *result;
    char *current_ptr;

    result = (struct data_packet*)malloc(sizeof(struct data_packet));
    result->ref_cnt = 1;
    current_ptr = dp;
    
    result->data_nr = atoi(current_ptr);
    current_ptr += strlen(current_ptr) + 1;

    result->data_size = atoi(current_ptr);
    current_ptr += strlen(current_ptr) + 1;

    result->data = (char*)calloc(result->data_size, sizeof(char));
    memcpy(result->data, current_ptr, result->data_size);
    return result;
}
