#ifndef NADAJNIK_INFO_LIST_H
#define NADAJNIK_INFO_LIST_H
#include <pthread.h>
#include "nadajnik_info.h"

struct nadajnik_info_list {
    size_t current_size;
    size_t max_size;
    struct nadajnik_info** info;
    pthread_mutex_t mutex;
    size_t* request_cnt;
};

void nadajnik_info_list_init(struct nadajnik_info_list* list);

void nadajnik_info_list_destroy(struct nadajnik_info_list* list);

void nadajnik_info_list_lock(struct nadajnik_info_list* list);
void nadajnik_info_list_unlock(struct nadajnik_info_list* list);

void nadajnik_info_list_add_nadajnik(
        struct nadajnik_info_list* list, struct nadajnik_info* info);

int nadajnik_info_list_remove(struct nadajnik_info_list* list, size_t index);

int nadajnik_info_list_match_info(struct nadajnik_info_list* list, struct nadajnik_info* info);

#endif
