#ifndef CONTROL_PACKET_H
#define CONTROL_PACKET_H

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define CTRL_PACKET_ID_REQUEST 1
#define CTRL_PACKET_RESEND_REQUEST 2
#define CTRL_PACKET_MAX_SERIALIZED_SIZE 50

struct control_packet {
    int type;
    // Used only in resend request.
    int packet_id;
};

char* control_packet_serialize(struct control_packet* cp, size_t *result_len);
struct control_packet* control_packet_deserialize(char* cp);

#endif
