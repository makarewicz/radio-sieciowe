#ifndef DATA_PACKET_H
#define DATA_PACKET_H

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#define DATA_PACKET_MAX_SERIALIZED_SIZE 10000

struct data_packet {
    unsigned int data_nr;
    size_t data_size;
    char* data;
    // Reference cnt, used only internally to minimize lock time in data_queue.
    // Isn't used in serialization.
    unsigned int ref_cnt;
};

void data_packet_init(
        struct data_packet* dp, size_t data_size, unsigned int data_nr, char* data);

void data_packet_destroy(struct data_packet* dp);

char* data_packet_serialize(struct data_packet* dp, size_t* result_len);
struct data_packet* data_packet_deserialize(char* dp);

void data_packet_inc_ref(struct data_packet* dp);
void data_packet_dec_ref(struct data_packet* dp);

#endif
