#ifndef RESEND_QUEUE_H
#define RESEND_QUEUE_H

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

struct resend_queue_node {
    struct resend_queue_node *next;
    unsigned int id;
};

struct resend_queue {
    struct resend_queue_node *head;
    struct resend_queue_node *tail;
    pthread_mutex_t mutex;
    bool sending;
};

void resend_queue_init(struct resend_queue* q);
void resend_queue_clear(struct resend_queue* q);
void resend_queue_add(struct resend_queue* q, unsigned int id);
int resend_queue_add_with_lock(struct resend_queue* q, unsigned int id);
void resend_queue_lock(struct resend_queue* q);
void resend_queue_unlock(struct resend_queue* q);

#endif
