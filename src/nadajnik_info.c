#include "nadajnik_info.h"
#define ITOA_BUF_SIZE 20

void nadajnik_info_destroy(struct nadajnik_info* info) {
    free(info->application_name);
    free(info->name);
    free(info->mcast_addr);
    free(info->version);
    free(info);
    return;
}

char* nadajnik_info_serialize(struct nadajnik_info* info, size_t* result_len) {
    size_t application_name_len, name_len, mcast_addr_len, version_len, psize_len;
    char *result, *current_ptr, *psize_str;

    psize_str = (char*)calloc(ITOA_BUF_SIZE, sizeof(char));
    sprintf(psize_str, "%d", (int)info->psize);
    psize_len = strlen(psize_str);

    application_name_len = strlen(info->application_name);
    name_len = strlen(info->name);
    mcast_addr_len = strlen(info->mcast_addr);
    version_len = strlen(info->version);
    *result_len = application_name_len + name_len + mcast_addr_len + version_len + psize_len + 5;
    
    result = (char*)calloc(*result_len, sizeof(char));
    current_ptr = result;

    strcpy(current_ptr, info->application_name);
    current_ptr += application_name_len + 1;
    
    strcpy(current_ptr, info->name);
    current_ptr += name_len + 1;
    
    strcpy(current_ptr, info->mcast_addr);
    current_ptr += mcast_addr_len + 1;
    
    strcpy(current_ptr, info->version);
    current_ptr += version_len + 1;

    strcpy(current_ptr, psize_str);
    current_ptr += psize_len + 1;

    return result;
}

struct nadajnik_info* nadajnik_info_deserialize(char* info) {
    struct nadajnik_info *result;
    size_t application_name_len, name_len, mcast_addr_len, version_len;
    char *current_ptr;

    current_ptr = info;
    result = (struct nadajnik_info*)malloc(sizeof(struct nadajnik_info));
    
    application_name_len = strlen(current_ptr);
    result->application_name = (char*)calloc(application_name_len + 1, sizeof(char));
    strcpy(result->application_name, current_ptr);
    current_ptr += application_name_len + 1;

    name_len = strlen(current_ptr);
    result->name = (char*)calloc(name_len + 1, sizeof(char));
    strcpy(result->name, current_ptr);
    current_ptr += name_len + 1;

    mcast_addr_len = strlen(current_ptr);
    result->mcast_addr = (char*)calloc(mcast_addr_len + 1, sizeof(char));
    strcpy(result->mcast_addr, current_ptr);
    current_ptr += mcast_addr_len + 1;

    version_len = strlen(current_ptr);
    result->version = (char*)calloc(version_len + 1, sizeof(char));
    strcpy(result->version, current_ptr);
    current_ptr += version_len + 1;

    result->psize = atoi(current_ptr);

    return result;
}

int nadajnik_info_cmp(struct nadajnik_info* lhs, struct nadajnik_info* rhs) {
    int r;
    if(lhs == NULL || rhs == NULL)
        return 1;

    r = strcmp(lhs->application_name, rhs->application_name);
    if (r != 0) {
        return r;
    }
    r = strcmp(lhs->name, rhs->name);
    if (r != 0) {
        return r;
    }
    r = strcmp(lhs->mcast_addr, rhs->mcast_addr);
    if (r != 0) {
        return r;
    }
    r = strcmp(lhs->version, rhs->version);
    if (r != 0) {
        return r;
    }
    return 0;
}
