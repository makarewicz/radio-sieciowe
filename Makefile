APPS    = nadajnik odbiornik
LIBS    = control_packet data_packet data_queue nadajnik_info nadajnik_info_list resend_queue util

SRCEXT   = c
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

ALL     = $(APPS) $(LIBS)

SRCS    := $(patsubst %, $(SRCDIR)/%.$(SRCEXT), $(ALL))
OBJS    := $(patsubst %, $(OBJDIR)/%.o, $(ALL))
LIBS_OBJ := $(patsubst %, $(OBJDIR)/%.o,$(LIBS))

DEBUG    = -g
INCLUDES = -I./inc
CFLAGS   = -Wall -pedantic -ansi -c -std=gnu99 $(DEBUG) $(INCLUDES)
LDFLAGS  = -lpthread

.PHONY: all clean distclean


all: $(BINDIR)/nadajnik $(BINDIR)/odbiornik

$(BINDIR)/nadajnik: buildrepo $(OBJS)
	@echo "Linking $@..."
	@$(CC) $(OBJDIR)/nadajnik.o $(LIBS_OBJ) $(LDFLAGS) -o $@

$(BINDIR)/odbiornik: buildrepo $(OBJS)
	@echo "Linking $@..."
	@$(CC) $(OBJDIR)/odbiornik.o $(LIBS_OBJ) $(LDFLAGS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	@echo "Compiling $<..."
	@$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) -r $(OBJDIR)

distclean: clean
	$(RM) -r $(BINDIR)

buildrepo:
	@$(call make-repo)

define make-repo
	mkdir -p $(OBJDIR); \
	mkdir -p $(BINDIR); 
endef


# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
  $(CC) -MM       \
        -MF $3    \
        -MP       \
        -MT $2    \
        $(CFLAGS) \
        $1
endef
